# coding: utf8
from __future__ import unicode_literals

# http://www.hottyres.ru/colors.html
colors_tuple = (
    ('+CH', '', 'Хромированные вставки на спицах'),
    ('AGF', 'Aluminate Gold Full Polish', 'Золотой с алюминиевой крошкой, полностью полированный'),
    ('AGL', 'Aluminate Gold Lip Polish', 'Золотой с алюминиевой крошкой, полированный обод'),
    ('BFP', '', 'Yamato – Матово-черный с полированной лицевой поверхностью'),
    ('BG', 'Black Green', 'Черно-зеленый'),
    ('BGL', 'Black Green Lip Polish', 'Черно-зеленый с полированным ободом'),
    ('B', 'Black', 'Черный'),
    ('BK', 'Black', 'Черный'),
    ('BLACK', 'Black', 'Черный'),
    ('BK/FP', 'Black Full Polish', 'Черный полностью полированный'),
    ('BK/P', 'Black Lip Polish', 'Черный с полированным ободом'),
    ('BK/PGD', 'Black Lip Polish Gold', 'Черный с золотистым полированным ободом'),
    ('BKF', 'Black Full Polish', 'Черный полностью полированный'),
    ('BKL', 'Black Lip Polish', 'Черный с полированным ободом'),
    ('BKRL', 'Black Red Line', 'Черный с красной линией на ободе'),
    ('BL/P', 'Blue Full Polish', 'Голубой с полированным ободом'),
    ('Black', 'Black', 'Черный'),
    ('BLF', 'Blue Full Polish', 'Голубой с полированным ободом'),
    ('Blue', 'Blue', 'Голубой'),
    ('BMF', '', 'Черный с алмазной механической обработкой лицевой поверхности'),
    ('BR', 'Brown; Bronze', 'Коричневый; иногда – Бронзовый'),
    ('BRL', 'Brown Lip Polish', 'Коричневый с полированным ободом'),
    ('BZ', 'Bronze', 'Бронзовый'),
    ('BZF', 'Bronze Full Polish', 'Бронзовый полностью полированный'),
    ('BZSCL', 'Bronze Shining Center Lip Polish', 'Бронзовый глянцевый с полированной ступицей и ободом'),
    ('BZSF', 'Bronze Shining Full Polish', 'Бронзовый глянцевый полностью полированный'),
    ('BZSL', 'Bronze Shining Lip Polish', 'Бронзовый глянцевый с полированным ободом'),
    ('CH', 'Chrome', 'Хромированный'),
    ('D/P', 'Diamond Polish', 'Серебристый с алмазной полировкой обода'),
    ('DB/P', 'Diamond Black Lip Polish', 'Черный с алмазной полировкой обода'),
    ('DBK', 'Diamond Black', 'Черный с алмазной пылью'),
    ('DBKL', 'Diamond Black Lip Polish', 'Черный с алмазной полировкой обода'),
    ('DGD/P', 'Diamond Gold Lip Polish', 'Золотистый с полированным ободом'),
    ('DGMF', 'Diamond Gun Metal Full Polish', 'Черный матовый с алмазной пылью полностью полированный'),
    ('FBKF', 'Flash Black Full Polish', 'Черный полностью полированный'),
    ('FCG', 'Flash Champagne Gold', 'Золотистые брызги шампанского'),
    ('FCGF', 'Flash Champagne Gold Full Polish', 'Золотистые брызги шампанского, полностью полированный'),
    ('FCGL', 'Flash Champagne Gold Lip Polish', 'Золотистые брызги шампанского с полированным ободом'),
    ('FGMF', 'Flash Gun Metal Full Polish', 'Черный матовый, полностью полированный'),
    ('FGRF', 'Flash Gray Full Polish', 'Серый частично полированный'),
    ('FSF', 'Flash Silver Full Polish', 'Серебристый, полностью полированный'),
    ('G', 'Gold; Graphite', 'Золотой; иногда – Графитовый'),
    ('G/P', 'Gold Lip Polish', 'Золотистый с полированным ободом'),
    ('GM', 'Gun Metal', 'Черный матовый'),
    ('GM:REPLICA', 'Gun Metal', 'Насыщенный темно-серый'),
    ('GM/FP', 'Gun Metal Full Polish', 'Черный матовый, полностью полированный'),
    ('GMCF', 'Gun Metal Center Full Polish', 'Черный матовый с полированной ступицей и лицевой стороной'),
    ('GMCL', 'Gun Metal Center Lip Polish', 'Черный матовый с полированной лицевой стороной'),
    ('GMF', 'Gun Metal Full Polish', 'Черный матовый полностью полированный'),
    ('GMF:REPLICA', 'Gun Metal Full Polish', 'Насыщенный темно-серый полностью полированный'),
    ('GMFP', 'Gun Metall Face Polished', 'Темно-серебристый с полированными спицами и ободом'),
    ('GML', 'Gun Metal Lip Polish', 'Черный матовый с полированным ободом'),
    ('GR', 'Grey; Graphite', 'Серый; иногда – Графитовый'),
    ('GR/P', 'Grey Full Polish', 'Серый с полированным ободом'),
    ('GRC', 'Gray Machined Face', 'Серый с полированными ступицей и ободом'),
    ('HB', 'Hyper Black', 'Насыщенный черный; иногда – Темно-серебристый'),
    ('HP', 'Hyper Silver', 'Насыщенный серебристый'),
    ('HP/Blue', 'Hyper Blue', 'Насыщенный голубой'),
    ('HP/Gold', 'Hyper Gold', 'Насыщенный золотистый'),
    ('HP/Grey', 'Hyper Grey', 'Темно-серый'),
    ('HPB', 'Hyper Black', 'Насыщенный черный'),
    ('HPB:REPLICA', 'Hyper Black', 'Насыщенный темно-серебристый'),
    ('HPBCL', 'Hyper Black Center Lip Polish', 'Насыщенный черный с полированными ступицей и ободом'),
    ('HPBF', 'Hyper Black Full Polish', 'Насыщенный черный, полностью полированный'),
    ('HPBL', 'Hyper Black Lip Polish', 'Насыщенный черный с полированным ободом'),
    ('HPCL', 'Hyper Silver Center Lip Polish', 'Насыщенный серебристый с полированными ступицей и ободом'),
    ('HPL', 'Hyper Silver Lip Polish', 'Насыщенный серебристый с полированным ободом'),
    ('HPT', 'Hyper Titanium', 'Насыщенный титановый'),
    ('HPT/DP', 'Hyper Titanium Diamond Lip Polish', 'Насыщенный титановый с алмазной полировкой обода'),
    ('HS', 'Hyper Silver', 'Насыщенный серебристый'),
    ('IMP', '', 'Ионовое покрытие'),
    ('IMP-BL/ST', '', 'Ионовое покрытие, голубой со стальной полированной оправой обода'),
    ('IMP-GD/ST', '', 'Ионовое покрытие, золотой со стальной полированной оправой обода'),
    ('LBR', 'Light Brown', 'Светло-коричневый'),
    ('LG', 'Light Gold', 'Золотистый'),
    ('LGF', 'Light Gold Full Polish', 'Золотистый полностью полированный'),
    ('LGL', 'Light Gold Lip Polish', 'Золотистый с полированным ободом'),
    ('LML', 'Light Metal Lip Polish', 'Светлый металлик с полированным ободом'),
    ('MB', 'Mist Black; Mashined Black', 'Черный с дымкой; иногда – Черный с полированной лицевой поверхностью'),
    ('MBCL', 'Mist Black Center Lip Polish', 'Черный с дымкой с полированной ступицей и ободом'),
    ('MBF', 'Mist Black Full Polish', 'Черный с дымкой полностью полированный'),
    ('MBFP', '', 'Yamato – Матово-черный с полированной лицевой поверхностью'),
    ('MBK', 'Mist Black', 'Черный с дымкой; Yamato – Зеркально-серебристый с зачернением'),
    ('MBL', 'Mist Black Lip Polish', 'Черный с дымкой с полированным ободом'),
    ('MBLPCP', '', 'Yamato – Черный матовый частично полированный'),
    ('MBR', 'Mist Brown', 'Коричневый с дымкой'),
    ('MGL', 'Mist Gold Lip Polish', 'Золотистый с дымкой с полированным ободом'),
    ('MGM', 'Mist Gun Metal', 'Черный матовый с дымкой'),
    ('MGM:REPLICA', 'Mist Gun Metal', 'Насыщенный темно-серый с дымкой'),
    ('MLG', 'Mist Light Gold', 'Светло-золотистый с дымкой'),
    ('MLM', 'Mist Light Metal', 'Светлый металлик с дымкой'),
    ('MS', 'Mist Silver', 'Серебристый с дымкой'),
    ('MS:REPLICA', 'Machine Surface', 'Серебристый c полированной лицевой стороной'),
    ('MW', 'Matt White', 'Белый матовый'),
    ('PW', 'Pearl White', 'Белый перламутровый'),
    ('PWL', 'Pearl White Lip Polish', 'Белый перламутровый с полированным ободом'),
    ('RBK', 'Rainbow Black', 'Черный переливающийся'),
    ('RED/P', 'Red Lip Polish', 'Красный с полированным ободом'),
    ('RF', 'Red Full Polish', 'Красный полностью полированный'),
    ('RGM', 'Rainbow Gun Metal', 'Черный матовый переливающийся'),
    ('S', 'Silver', 'Серебристый'),
    ('SILVER', 'Silver', 'Серебристый'),
    ('S11', '', 'Светло-серебристый'),
    ('S2', '', 'Темно-серебристый'),
    ('SAP', 'Silver Acril Powder', 'Серебристый с акриловым напылением'),
    ('SAPF', 'Silver Acril Full Polished', 'Серебристый с акриловым напылением, полностью полированный'),
    ('SF', 'Silver Full Polish', 'Серебристый полностью полированный'),
    ('SHB', '', 'Yamato – Темно-серый'),
    ('SL', 'Silver Lip Polish', 'Серебристый с полированным ободом'),
    ('SP', 'Sand Polish; Silver Polish', 'Песочный; иногда – Серебристый с полированной лицевой поверхностью'),
    ('SS', 'Shining Silver', 'Сверкающее серебро (перламутр)'),
    ('SS/P', 'Shining Silver Lip Polish', 'Сверкающее серебро (перламутр) с полированым ободом'),
    ('SY/P', 'Shining Yellow Lip Polish', 'Ярко-желтый с полированным ободом'),
    ('T/G', 'Titanium Gold', 'Титановый золотой'),
    ('W', 'White', 'Белый'),
    ('W/P', 'White Lip Polish', 'Белый с полированным ободом'),
    ('WF', 'White Full Polish', 'Белый полностью полированный'),
    ('White', 'White', 'Белый'),
    ('WL', 'White Lip Polish', 'Белый с полированным ободом'),
    ('WSS', 'White Shining Silver', 'Осветленное сверкающее серебро (перламутр)'),
)

colors = [(abbr, rus) for abbr, eng, rus in colors_tuple]

colors_dict = dict(colors)

