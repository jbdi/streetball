# coding: utf-8
import csv
from datetime import datetime

from django.core.management.base import BaseCommand
from django.conf import settings

import pytz

from cpa.koleso.models import CatalogVendor as Vendor, CatalogCategory as Category, Sammy


# """
# Скачивание файла
# Импорт файла
#     - подсчет кол-ва товаров в файле
# """

ROOT_CATEGORY_ID = 244

class Command(BaseCommand):
    # сделать отдельной процедурой подсчет(перерасчет) кол-ва товаров в категории
    def handle(self, *args, **options):
        csv_reader('sammy-osnovnoi_products_20160302_080429.csv', run_import)
        # print "amount: %d, created: %d, updated: %d" % (cnt, created_cnt, updated_cnt)


def run_import(row, model=Sammy):
    ALLOWED_FIELDS = (
        'available',
        'category_id',
        'condition',
        'country_of_origin',
        'delivery',
        'dimension_unit',
        'item_id',
        'modified_time',
        'param',
        'picture',
        'price',
        'oldprice',
        'quantity',
        'shipping_from',
        'topseller',
        'translation_required',
        'item_type',
        'url',
        'vendor',
        'video_url',
        'weight_unit',
        'delivery',
        'adult',
        'model',
        'pickup',
        'name',

        # float fields
        'product_weight',
        'package_height',
        'package_length',
        'package_weight',
        'package_width',
        'international_shipping_cost',
        'international_shipping_cost_by_country_br',
        'merchant_shipping_cost',
        # 'local_delivery_cost',
    )

    row_columns = row.keys()
    for field_name in row_columns:
        if not field_name in ALLOWED_FIELDS:
            del row[field_name]

    vendor, _ = Vendor.objects.get_or_create(name=row['vendor'].strip().capitalize())
    row['vendor'] = vendor

    row['product_weight'] = float(row['product_weight'] or 0)
    row['package_height'] = float(row['package_height'] or 0)
    row['package_length'] = float(row['package_length'] or 0)
    row['package_weight'] = float(row['package_weight'] or 0)
    row['package_width'] = float(row['package_width'] or 0)
    row['international_shipping_cost'] = float(row['international_shipping_cost'] or 0)
    row['international_shipping_cost_by_country_br'] = float(row['international_shipping_cost_by_country_br'] or 0)
    row['merchant_shipping_cost'] = float(row['merchant_shipping_cost'] or 0)
    # row['local_delivery_cost'] = float(row['local_delivery_cost'] or 0)

    row['oldprice'] = float(row['oldprice'] or 0)
    row['price'] = float(row['price'] or 0)
    row['modified_time'] = datetime.utcfromtimestamp(int(row['modified_time'])).replace(tzinfo=pytz.utc)
    row['category_id'] = make_categories_tree(row['category_id'])
    row['enabled'] = True

    row = {col: unicode(val, 'utf-8') if isinstance(val, str) else val for col, val in row.items()}
    item, created = model.objects.update_or_create(item_id=row['item_id'], defaults=row)
    counters = {}
    counters['handled'] = 1
    if created:
        counters['created'] = 1
        counters['updated'] = 0
    else:
        counters['created'] = 0
        counters['updated'] = 1
    return counters


def make_categories_tree(categories_string=''):
    """
    делает из строки "Category/subcategory/sub subcategory" дерево категорий в бд
    """
    parent = Category.objects.get(id=ROOT_CATEGORY_ID)
    categories = enumerate(categories_string.split('/'), start=0)
    for index, category_name in categories:
        category, created = Category.objects.get_or_create(name=category_name, parent=parent)
        parent = category
    return category.id


def csv_reader(file_name, row_handler):
    file_path = settings.BASE_DIR + '/' + file_name

    with open(file_path, 'r') as line:
        reader = csv.DictReader(line, delimiter=';')

        # # также можно припихнуть проверку на слишком маленкое кол-во товаров
        # cursor.execute('truncate catalog_item')
        handled = 0
        created = 0
        updated = 0
        for row in reader:
            counters = row_handler(row)
            handled += counters['handled']
            created += counters['created']
            updated += counters['updated']
        print "handled: %d, created: %d, updated: %d" % (handled, created, updated)
