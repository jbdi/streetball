# coding: utf-8
# from __future__ import unicode_literals
import csv
from datetime import datetime
import re
import sys
from os import path

from django.core.management.base import BaseCommand
from django.conf import settings

import pytz

from cpa.koleso.models import (CatalogVendor as Vendor, CatalogCategory as Category,
        Tire, Disk, Goods, TireVendor, DiskVendor, GoodsVendor)

# """
# Скачивание файла
# Импорт файла
#     - подсчет кол-ва товаров в файле
# """

ROOT_CATEGORY_ID = 1
DISK_CATEGORY_ID = 2
TIRE_CATEGORY_ID = 3

IMPORTED_ITEMS_ID = []

class Command(BaseCommand):
    # сделать отдельной процедурой подсчет(перерасчет) кол-ва товаров в категории
    def handle(self, *args, **options):
        csv_file = path.join(settings.BASE_DIR, 'koleso.csv')
        csv_reader(csv_file, run_import, **options)

        Disk.objects.exclude(id__in=IMPORTED_ITEMS_ID).update(enabled=False)
        Tire.objects.exclude(id__in=IMPORTED_ITEMS_ID).update(enabled=False)
        Goods.objects.exclude(id__in=IMPORTED_ITEMS_ID).update(enabled=False)



def run_import(row, **options):
    root, _ = Category.objects.get_or_create(name='root_koleso')
    disk, _ = Category.objects.get_or_create(name=u'Колесные диски', parent=root)
    tire, _ = Category.objects.get_or_create(name=u'Автошины', parent=root)

    ALLOWED_FIELDS = (
        'available',
        'picture',
        'vendor',
        'name',
        'oldprice',
        'price',
        'url',
        'modified_time',
        'category_id',
        'sales_notes',
        'item_type',
        'item_id',
        'local_delivery_cost',
        'manufacturer_warranty',
        # 'delivery',
        # 'pickup',
        # 'store',
    )

    row_columns = row.keys()
    for field_name in row_columns:
        if not field_name in ALLOWED_FIELDS:
            del row[field_name]


    row['category_id'] = make_categories_tree(row['category_id'])
    if row['category_id'] == DISK_CATEGORY_ID:
        disks_params = get_disks_params(row)
        if disks_params:
            row.update(disks_params)
        else:
            row = None
        model = Disk
        vendor_model = DiskVendor
    elif row['category_id'] == TIRE_CATEGORY_ID:
        tire_params = get_tire_params(row)
        if tire_params:
            row.update(tire_params)
        else:
            row = None
        model = Tire
        vendor_model = TireVendor
    else:
        model = Goods
        vendor_model = GoodsVendor
        row['vendor'] = row['vendor'] or 'Empty'

    if not row:
        return {'created': 0, 'updated': 0, 'handled': 1}

    vendor, _ = vendor_model.objects.get_or_create(name=row['vendor'].strip().capitalize())
    row['vendor'] = vendor
    row['price'] = float(row['price'].replace(',', '.') or 0)
    row['enabled'] = True
    row['oldprice'] = float(row['oldprice'].replace(',', '.') or 0)
    row['modified_time'] = datetime.utcfromtimestamp(int(row['modified_time'])).replace(tzinfo=pytz.utc)
    if row['category_id'] in {DISK_CATEGORY_ID, TIRE_CATEGORY_ID}:
        row['manufacturer_warranty'] = True if row['manufacturer_warranty'] == 'true' else None
    else:
        del row['local_delivery_cost']
        del row['manufacturer_warranty']

    row = {col: val for col, val in row.items()}

    try:
        item, created = model.objects.update_or_create(item_id=row['item_id'], defaults=row)
        IMPORTED_ITEMS_ID.append(item.id)
    except Exception, e:
        print 'error with %s' % row.get('item_id')
        created = False

    counters = {}
    counters['handled'] = 1
    if created:
        counters['created'] = 1
        counters['updated'] = 0
    else:
        counters['created'] = 0
        counters['updated'] = 1
    return counters


def get_disks_params(row):
    preg_all = ur'^(replica)?([\w\d\.\-\s]+?)?\s?(replica)?\s?([\w\d\.\-]+)\s?(:?[\d\.]+)(x|\*)?([\d\.]+)\/([\d\.]+)(x|\*)?([\d\.]+)\s+((ет|et)-?[\d\.]+)\s+(d[\d\.]+)\s?([\w\+]+)$'
    preg_without_model = ur'^(replica)?([\w\d\.\-\s]+?)?\s?(replica)?\s?(:?[\d\.]+)(x|\*)?([\d\.]+)\/([\d\.]+)(x|\*)?([\d\.]+)\s+((ет|et)-?[\d\.]+)\s+(d[\d\.]+)\s?([\w\+]+)$'
    params = re.search(preg_all, unicode(row['name'], 'utf8'), flags=re.U|re.I)
    preg_all_is_ok = True

    if params and not is_number(params.groups()[4]): # check width
        preg_all_is_ok = False
        params = re.search(preg_without_model, unicode(row['name'], 'utf8'), flags=re.U|re.I)

    if params:
        if preg_all_is_ok:
            is_replica, brand, is_replica2, disk_model, width, _, diameter, pcd_part1, pcd_sep, pcd_part2, edge, _, hole_diameter, color = params.groups()
        else:
            is_replica, brand, is_replica2, width, _, diameter, pcd_part1, pcd_sep, pcd_part2, edge, _, hole_diameter, color = params.groups()
        if brand and width and diameter and color:
            try:
                row['color'] = color.upper()
                row['replica'] = bool(is_replica or is_replica2)
                row['model'] = ''
                if preg_all_is_ok:
                    row['model'] = disk_model
                row['width'] = float(get_number_from_str(width))
                row['diameter'] = float(diameter)
                row['pcd_part1'] = pcd_part1
                row['pcd_part2'] = float(get_number_from_str(pcd_part2))
                row['edge'] = float(get_number_from_str(edge))
                row['hole_diameter'] = float(get_number_from_str(hole_diameter))
                row['local_delivery_cost'] = float(row['local_delivery_cost']) if row['local_delivery_cost'] else 0.0
            except ValueError:
                return
            return row


def get_tire_params(row):
    params = re.search(ur'^(.*?)\s?(([\d\.]+)\/)?([\d\.]+)(z?R([\d\.]+)([\w\d]+)?)+\s?(([\d\.]+)\/?([\d\.]+)?\s?(j|k|l|m|n|p|q|r|s|t|т|u|h|v|vr|w|y|zr)?)?(.*)?$', unicode(row['name'], 'utf8'), flags=re.U|re.I)
    # named_params = ur'^(?P<title>.*?)\s?((?P<width>[\d\.]+)\/)?(?P<height>[\d\.]+)(z?R(?P<diameter>[\d\.]+)(?P<diameter_param>[\w\d]+)?)+\s?((?P<load_index1>[\d\.]+)\/?(?P<load_index2>[\d\.]+)?\s?(?P<speed_index>j|k|l|m|n|p|q|r|s|t|т|u|h|v|vr|w|y|zr)?)?(?P<other_data>.*)?'
    if params:
        title, _, width, height, _, diameter, diameter_param, _, load_index1, load_index2, speed_index, other_data = params.groups()
        if width and diameter and speed_index:
            row['title'] = title
            row['width'] = width
            row['height'] = height
            row['diameter'] = float(diameter)
            row['diameter_param'] = diameter_param
            row['load_index1'] = load_index1
            row['load_index2'] = load_index2
            row['speed_index'] = speed_index.upper() if speed_index else None
            row['local_delivery_cost'] = float(row['local_delivery_cost']) if row['local_delivery_cost'] else 0.0

            spikes = re.search(ur'(о?шип)', other_data, re.I|re.U)
            row['spikes'] = bool(spikes)

            xl = re.search(ur'(XL|Run Flat|rf)', other_data, re.I|re.U)
            row['xl'] = bool(xl)

            tl = re.search(ur'(Tl|Б\/К)', other_data, re.I|re.U)
            row['tl'] = bool(tl)

            eco = re.search(ur'(eco)', other_data, re.I|re.U)
            row['eco'] = bool(eco)

            dirty = re.search(ur'(M\+S|M\&S)', other_data, re.I|re.U)
            row['dirty'] = bool(dirty)

            ssr = re.search(ur'(ssr)', other_data, re.I|re.U)
            row['ssr'] = bool(ssr)
            return row


def get_number_from_str(string):
    return re.split('([\d\.]+)', string, maxsplit=1, flags=re.U|re.I)[1] or 0


def make_categories_tree(categories_string=''):
    """
    делает из строки "Category/subcategory/sub subcategory" дерево категорий в бд
    """
    parent = Category.objects.get(id=ROOT_CATEGORY_ID)
    categories = enumerate(categories_string.split('/'), start=0)
    for index, category_name in categories:
        category, created = Category.objects.get_or_create(name=category_name, parent=parent)
        parent = category
    return category.id


def csv_reader(file_path, row_handler, **options):
    verbosity = options.get('verbosity', 0)

    with open(file_path, 'r') as line:
        reader = csv.DictReader(line, delimiter=';')

        # # также можно припихнуть проверку на слишком маленкое кол-во товаров
        # cursor.execute('truncate catalog_item')
        handled = 0
        created = 0
        updated = 0
        for row in reader:
            counters = row_handler(row, **options)
            handled += counters['handled']
            created += counters['created']
            updated += counters['updated']
            if verbosity == 2:
                sys.stdout.write("\rhandled: %d, created: %d, updated: %d" % (handled, created, updated))
                sys.stdout.flush()
        if verbosity == 1:
            sys.stdout.write("\rhandled: %d, created: %d, updated: %d" % (handled, created, updated))


def is_number(data):
    try:
        float(data) # for int, long and float
    except ValueError:
        try:
            complex(data) # for complex
        except ValueError:
            return False
    return True
