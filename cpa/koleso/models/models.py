# coding: utf8
import re

from django.db import models
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse

from cpa.koleso.models.base import BaseCatalogItem
from cpa.koleso.colors import colors_dict
from cpa.helpers.templatetags.helpers import plural

SPEED_INDEX_CHOICES = (
    ('J', '100'),
    ('K', '110'),
    ('L', '120'),
    ('M', '130'),
    ('N', '140'),
    ('P', '150'),
    ('Q', '160'),
    ('R', '170'),
    ('S', '180'),
    ('T', '190'),
    ('Т', '190'),
    ('U', '200'),
    ('H', '210'),
    ('V', '240'),
    ('VR', '>210'),
    ('W', '270'),
    ('Y', '300'),
    ('ZR', '>240')
)


"""
Про ШИНЫ

отделить грузовые шины

Сезонность
Шипы
Ширина
Высота
Диаметр
Индекс скорости
Индекс нагрузки
Объем


Европейские типоразмеры шин и другие...

HANKOOK VANTRA LT RA18 205/65 R16C 103/101T KR/HU
Hankook Vantra     Lt             Ra18       205/65R16c (155R12c)                              107/105T (или 105T)                                         Kr/Hu
[   название  ]  [?Light Truck?] [модель!]   [width 205mm/height 65% от ширины, радиус 16с"]   [индекс нагрузки/индекс нагрузки(буква - индекс скорости)] [Страна]

# z? непонятно, что за параметр, но он есть тут Sailun 255/35Zr19 96Y Atrezzo Zsr XL
^(.*?)\s?(([\d\.]+)\/)?([\d\.]+)(z?R([\d\.]+)\w?)+\s?([\d\.]+)\/?([\d\.]+)?(j|k|l|m|n|p|q|r|s|t|u|h|v|vr|w|y|zr)?\s?(шип)?

индексы нагрузки и всякое http://www.chelshina.ru/pages/page8.htm http://kolobox.ru/vse-o-shine/markirovka_shin/
ОБОЗНАЧЕНИЯ НА ШИНАХ в партнерке http://ufa.kolesatyt.ru/goodinfo/oboznacheniya-na-shinakh/
интересные параметры попадаются http://www.ityre.com/ru/tyres/catalog/hankook/model/27424/

можно попробвать составить таблицу соответствия шин и колес к маркам авто
есть pdf файлы http://www.avtoall.ru/info/pcd/180.pdf

Можно сделать подбор дисков для шин и наоборот по ТАБЛИЦЕ СООТВЕТСТВИЯ ШИН И ДИСКОВ
http://aset.by/info/~shownews/tablicha_sootwetstwiya_schin_i_diskow

Сграбить http://www.vianor-tyres.ru/podbor_po_avto/
и сделать подбор шин и дисков по марке авто

Вообще, можено много всего натырить на сайте http://www.vianor-tyres.ru/gruzovyeshiny/
"""


DISKS_CATEGORY_ID = 2


class GoodsVendor(models.Model):
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'catalog_goodsvendor'
        ordering = ('name',)

    def get_absolute_url(self):
        return reverse('koleso:list', args=['goods', self.name])


class TireVendor(models.Model):
    name = models.CharField(max_length=128)
    rus_name = models.CharField(max_length=128, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    sorting_weight = models.PositiveSmallIntegerField(default=0)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'catalog_tirevendor'
        ordering = ('-sorting_weight', 'name',)

    def get_absolute_url(self):
        return reverse('koleso:list', args=['tires', self.name])


class DiskVendor(models.Model):
    name = models.CharField(max_length=128)
    rus_name = models.CharField(max_length=128, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    sorting_weight = models.PositiveSmallIntegerField(default=0)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'catalog_diskvendor'
        ordering = ('-sorting_weight', 'name',)

    def get_absolute_url(self):
        return reverse('koleso:list', args=['disks', self.name])


class Goods(BaseCatalogItem):
    vendor = models.ForeignKey('GoodsVendor', related_name='items', blank=True, null=True)

    class Meta:
        db_table = 'catalog_goods'
        ordering = ('price',)


class Tire(BaseCatalogItem):
    vendor = models.ForeignKey('TireVendor', related_name='items', blank=True, null=True)
    local_delivery_cost = models.FloatField(max_length=12, blank=True, null=True, default=0)
    manufacturer_warranty = models.NullBooleanField()
    store_id = models.PositiveSmallIntegerField(blank=True, null=True)
    delivery = models.CharField(max_length=512, null=True, blank=True) # bool
    title = models.CharField(max_length=128, blank=True, null=True)
    width = models.FloatField(max_length=4, blank=True, null=True, default=0)
    height = models.FloatField(max_length=4, blank=True, null=True, default=0)
    diameter = models.FloatField(max_length=4, blank=True, null=True, default=0)
    diameter_param = models.CharField(max_length=32, blank=True, null=True)
    load_index1 = models.FloatField(max_length=4, blank=True, null=True, default=0)
    load_index2 = models.FloatField(max_length=4, blank=True, null=True, default=0)
    speed_index = models.CharField(max_length=32, blank=True, null=True,
                                   choices=SPEED_INDEX_CHOICES, default=None)
    spikes = models.NullBooleanField(u'шипы')
    xl = models.NullBooleanField(u'с укрепленным каркасом')
    tl = models.NullBooleanField(u'бескамерная')
    eco = models.NullBooleanField()
    dirty = models.NullBooleanField(u'езда по снегу или грязи')
    ssr = models.NullBooleanField(u'шина с укрепленной боковиной ранфлэт')


    class Meta:
        db_table = 'catalog_tire'
        ordering = ('price',)

    def get_absolute_url(self):
        return reverse('koleso:detail', args=['tires', self.vendor.name, self.pk])

    def get_speed_index_display(self):
        return SPEED_INDEX_CHOICES.get(self.speed_index)

    def link_title(self):
        vendor = self.vendor.name or self.vendor.rus_name
        title = self.title.replace(vendor, '<a href="%s">%s</a>' % (self.vendor.get_absolute_url(), vendor))
        return mark_safe(title)

    @property
    def text(self):
        inches = [u'дюйм', u'дюйма', u'дюймов']

        spikes = u'Шипованная' if self.spikes else ''
        xl = u'с укрепленным каркасом' if self.xl else ''
        tl = u'бескамерная' if self.tl else ''
        dirty = u' предназначена для езды по снегу или грязи' if self.dirty else ''
        ssr = u'с укрепленной боковиной ранфлэт' if self.ssr else ''
        diameter = u'Посадочный диаметр диска %s %s.<br>' % (self.diameter, plural(self.diameter, *inches)) if self.diameter else ''
        xl_ssr = ', %s' % xl or ssr if xl or ssr else ''

        text = u"""
        {0} {1} автомобильная шина  {2}{3}{4}.<br>{5}Максимально допустимая скорость езды {6} км/ч.
        """.format(spikes, tl, self.title, xl_ssr, dirty, diameter, self.get_speed_index_display())
        return mark_safe(text.strip())


class Disk(BaseCatalogItem):
    vendor = models.ForeignKey('DiskVendor', related_name='items', blank=True, null=True)
    local_delivery_cost = models.FloatField(max_length=12, blank=True, default=0)
    delivery = models.CharField(max_length=512, blank=True) # bool
    color = models.CharField(max_length=32, blank=True, null=True)
    replica = models.NullBooleanField()
    model = models.CharField(max_length=32, blank=True, null=True)
    width = models.FloatField(max_length=12, blank=True, default=0)
    diameter = models.FloatField(max_length=12, blank=True, default=0)
    pcd_part1 = models.PositiveSmallIntegerField(blank=True, default=0)
    pcd_part2 = models.FloatField(max_length=12, blank=True, default=0)
    edge = models.FloatField(max_length=12, blank=True, default=0)
    hole_diameter = models.FloatField(max_length=12, blank=True, default=0)
    manufacturer_warranty = models.NullBooleanField()
    store_id = models.PositiveSmallIntegerField(blank=True, null=True)


    class Meta:
        db_table = 'catalog_disk'
        ordering = ('price',)


    def get_absolute_url(self):
        return reverse('koleso:detail', args=['disks', self.vendor.name, self.pk])

    @property
    def replica_title(self):
        if self.replica:
             # Убрал, неуверен, что пользователи понимают
            return '' # u'(качественная копия)'
        return u''

    @property
    def text(self):
        # _color, replica, brand, model, width, diameter,  pcd_part1, pcd_part2, hole_diameter, edge
        inches = [u'дюйм', u'дюйма', u'дюймов']
        text = u"""Автомобильный диск %s %s %s шириной %s %s и диаметром %s %s.<br>
                   Крепежных болтов - %s / радиус болтов - %sмм.<br>
                   Диаметр центрального отверстия диска %sмм, вылет %sмм.""" \
               % (self.replica_title, self.vendor.rus_name or self.vendor.name, self.model, self.width, plural(self.width, *inches), self.diameter, plural(self.diameter, *inches), self.pcd_part1, self.pcd_part2, self.hole_diameter, self.edge)
        return mark_safe(text)

    @property
    def translated_color(self):
        # цветов в расшифровке может быть много, решить это
        colors = self.color.split('+')
        _get_color = lambda color, is_replica: '%s:REPLICA' % color if is_replica else color
        translated_colors = [colors_dict.get(_get_color(color.upper(), self.replica), color) for color in colors]
        return ' + '.join(translated_colors)

    @property
    def title(self):
        title = u'Диск %s %s %s' % (self.replica_title, self.vendor.rus_name or self.vendor.name, self.model)
        return mark_safe(title)

    def link_title(self):
        title = u'<a href="%s">Диск %s %s</a> %s' % (self.vendor.get_absolute_url(), self.replica_title, self.vendor.rus_name or self.vendor.name, self.model)
        return mark_safe(title)






