# coding: utf-8
from django.contrib import admin
from django.contrib.admin import RelatedFieldListFilter

from django_mptt_admin.admin import DjangoMpttAdmin

from cpa.koleso.models import (
    TireVendor, DiskVendor, CatalogCategory,
    Disk, Tire, Goods)


class DropdownFilter(RelatedFieldListFilter):
    template = 'admin/dropdown_filter.html'
    # NOTE: припилить отображение товаров из всех подкатегорий


@admin.register(CatalogCategory)
class CategoryAdmin(DjangoMpttAdmin):
    pass


@admin.register(TireVendor)
class TireVendorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'rus_name', 'sorting_weight')
    list_editable = ('name', 'rus_name', 'sorting_weight')


@admin.register(DiskVendor)
class DiskVendorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'rus_name', 'sorting_weight')
    list_editable = ('name', 'rus_name', 'sorting_weight')


@admin.register(Tire)
class TireAdmin(admin.ModelAdmin):
    list_display = ('name', 'vendor', 'category', 'param', 'price', 'discount_percent')
    list_filter = (
        ('category', DropdownFilter),
    )

@admin.register(Disk)
class DiskAdmin(admin.ModelAdmin):
    list_display = ('name', 'vendor', 'category', 'param', 'price', 'discount_percent')
    list_filter = (
        ('category', DropdownFilter),
    )

@admin.register(Goods)
class GoodsAdmin(admin.ModelAdmin):
    readonly_fields = ('url', 'country_of_origin')
    fields = (
        ('category',),
        ('price', 'oldprice'),
        ('name', 'item_id', 'description'),
        ('available', 'enabled'),
        ('vendor',),
        ('picture', 'url'),
        # others
        ('condition',),
        ('modified_time',),
        ('param',),
        (
            'sales_notes', 'item_type',
            'country_of_origin',
        ),
    )
    list_display = ('name', 'vendor', 'price')
    list_filter = ('category',)

