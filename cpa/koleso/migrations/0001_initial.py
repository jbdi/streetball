# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CatalogCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=100, blank=True)),
                ('sort_pos', models.IntegerField(default=0, null=True)),
                ('popular', models.BooleanField(default=False)),
                ('seo_name', models.CharField(default=b'', max_length=100, blank=True)),
                ('seo_title', models.CharField(default=b'', max_length=250, blank=True)),
                ('seo_description', models.CharField(default=b'', max_length=250, blank=True)),
                ('seo_keywords', models.CharField(default=b'', max_length=250, blank=True)),
                ('text', models.TextField(default=b'', blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='category', blank=True, to='koleso.CatalogCategory', null=True)),
            ],
            options={
                'db_table': 'catalog_category',
            },
        ),
        migrations.CreateModel(
            name='CatalogVendor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
            ],
            options={
                'db_table': 'catalog_vendor',
            },
        ),
        migrations.CreateModel(
            name='Disk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country_of_origin', models.CharField(max_length=100, null=True, blank=True)),
                ('condition', models.CharField(max_length=64, null=True, blank=True)),
                ('enabled', models.BooleanField(default=False)),
                ('available', models.BooleanField(default=False)),
                ('picture', models.URLField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('oldprice', models.FloatField(default=0, max_length=10, null=True, blank=True)),
                ('price', models.FloatField(default=0, max_length=10, null=True, blank=True)),
                ('modified_time', models.DateTimeField(null=True, blank=True)),
                ('url', models.URLField(max_length=512, null=True, blank=True)),
                ('param', models.CharField(max_length=512, null=True, blank=True)),
                ('sales_notes', models.CharField(max_length=512, null=True, blank=True)),
                ('item_type', models.CharField(max_length=512, null=True, blank=True)),
                ('item_id', models.CharField(default='', max_length=128, blank=True)),
                ('name', models.CharField(max_length=512, null=True, blank=True)),
                ('local_delivery_cost', models.FloatField(default=0, max_length=12, blank=True)),
                ('delivery', models.CharField(max_length=512, blank=True)),
                ('color', models.CharField(max_length=32, null=True, blank=True)),
                ('replica', models.NullBooleanField()),
                ('model', models.CharField(max_length=32, null=True, blank=True)),
                ('width', models.FloatField(default=0, max_length=12, blank=True)),
                ('diameter', models.FloatField(default=0, max_length=12, blank=True)),
                ('pcd_part1', models.PositiveSmallIntegerField(default=0, blank=True)),
                ('pcd_part2', models.FloatField(default=0, max_length=12, blank=True)),
                ('edge', models.FloatField(default=0, max_length=12, blank=True)),
                ('hole_diameter', models.FloatField(default=0, max_length=12, blank=True)),
                ('manufacturer_warranty', models.NullBooleanField()),
                ('store_id', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('category', models.ForeignKey(to='koleso.CatalogCategory', null=True)),
            ],
            options={
                'ordering': ('price',),
                'db_table': 'catalog_disk',
            },
        ),
        migrations.CreateModel(
            name='DiskVendor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('rus_name', models.CharField(max_length=128, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'catalog_diskvendor',
            },
        ),
        migrations.CreateModel(
            name='Goods',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country_of_origin', models.CharField(max_length=100, null=True, blank=True)),
                ('condition', models.CharField(max_length=64, null=True, blank=True)),
                ('enabled', models.BooleanField(default=False)),
                ('available', models.BooleanField(default=False)),
                ('picture', models.URLField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('oldprice', models.FloatField(default=0, max_length=10, null=True, blank=True)),
                ('price', models.FloatField(default=0, max_length=10, null=True, blank=True)),
                ('modified_time', models.DateTimeField(null=True, blank=True)),
                ('url', models.URLField(max_length=512, null=True, blank=True)),
                ('param', models.CharField(max_length=512, null=True, blank=True)),
                ('sales_notes', models.CharField(max_length=512, null=True, blank=True)),
                ('item_type', models.CharField(max_length=512, null=True, blank=True)),
                ('item_id', models.CharField(default='', max_length=128, blank=True)),
                ('name', models.CharField(max_length=512, null=True, blank=True)),
                ('category', models.ForeignKey(to='koleso.CatalogCategory', null=True)),
            ],
            options={
                'ordering': ('price',),
                'db_table': 'catalog_goods',
            },
        ),
        migrations.CreateModel(
            name='GoodsVendor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'catalog_goodsvendor',
            },
        ),
        migrations.CreateModel(
            name='Tire',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country_of_origin', models.CharField(max_length=100, null=True, blank=True)),
                ('condition', models.CharField(max_length=64, null=True, blank=True)),
                ('enabled', models.BooleanField(default=False)),
                ('available', models.BooleanField(default=False)),
                ('picture', models.URLField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('oldprice', models.FloatField(default=0, max_length=10, null=True, blank=True)),
                ('price', models.FloatField(default=0, max_length=10, null=True, blank=True)),
                ('modified_time', models.DateTimeField(null=True, blank=True)),
                ('url', models.URLField(max_length=512, null=True, blank=True)),
                ('param', models.CharField(max_length=512, null=True, blank=True)),
                ('sales_notes', models.CharField(max_length=512, null=True, blank=True)),
                ('item_type', models.CharField(max_length=512, null=True, blank=True)),
                ('item_id', models.CharField(default='', max_length=128, blank=True)),
                ('name', models.CharField(max_length=512, null=True, blank=True)),
                ('local_delivery_cost', models.FloatField(default=0, max_length=12, null=True, blank=True)),
                ('manufacturer_warranty', models.NullBooleanField()),
                ('store_id', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('delivery', models.CharField(max_length=512, null=True, blank=True)),
                ('title', models.CharField(max_length=128, null=True, blank=True)),
                ('width', models.FloatField(default=0, max_length=4, null=True, blank=True)),
                ('height', models.FloatField(default=0, max_length=4, null=True, blank=True)),
                ('diameter', models.FloatField(default=0, max_length=4, null=True, blank=True)),
                ('diameter_param', models.CharField(max_length=32, null=True, blank=True)),
                ('load_index1', models.FloatField(default=0, max_length=4, null=True, blank=True)),
                ('load_index2', models.FloatField(default=0, max_length=4, null=True, blank=True)),
                ('speed_index', models.CharField(default=None, max_length=32, null=True, blank=True, choices=[(b'J', b'100'), (b'K', b'110'), (b'L', b'120'), (b'M', b'130'), (b'N', b'140'), (b'P', b'150'), (b'Q', b'160'), (b'R', b'170'), (b'S', b'180'), (b'T', b'190'), (b'\xd0\xa2', b'190'), (b'U', b'200'), (b'H', b'210'), (b'V', b'240'), (b'VR', b'>210'), (b'W', b'270'), (b'Y', b'300'), (b'ZR', b'>240')])),
                ('spikes', models.NullBooleanField(verbose_name='\u0448\u0438\u043f\u044b')),
                ('xl', models.NullBooleanField(verbose_name='\u0441 \u0443\u043a\u0440\u0435\u043f\u043b\u0435\u043d\u043d\u044b\u043c \u043a\u0430\u0440\u043a\u0430\u0441\u043e\u043c')),
                ('tl', models.NullBooleanField(verbose_name='\u0431\u0435\u0441\u043a\u0430\u043c\u0435\u0440\u043d\u0430\u044f')),
                ('eco', models.NullBooleanField()),
                ('dirty', models.NullBooleanField(verbose_name='\u0435\u0437\u0434\u0430 \u043f\u043e \u0441\u043d\u0435\u0433\u0443 \u0438\u043b\u0438 \u0433\u0440\u044f\u0437\u0438')),
                ('ssr', models.NullBooleanField(verbose_name='\u0448\u0438\u043d\u0430 \u0441 \u0443\u043a\u0440\u0435\u043f\u043b\u0435\u043d\u043d\u043e\u0439 \u0431\u043e\u043a\u043e\u0432\u0438\u043d\u043e\u0439 \u0440\u0430\u043d\u0444\u043b\u044d\u0442')),
                ('category', models.ForeignKey(to='koleso.CatalogCategory', null=True)),
            ],
            options={
                'ordering': ('price',),
                'db_table': 'catalog_tire',
            },
        ),
        migrations.CreateModel(
            name='TireVendor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('rus_name', models.CharField(max_length=128, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'catalog_tirevendor',
            },
        ),
        migrations.AddField(
            model_name='tire',
            name='vendor',
            field=models.ForeignKey(related_name='items', blank=True, to='koleso.TireVendor', null=True),
        ),
        migrations.AddField(
            model_name='goods',
            name='vendor',
            field=models.ForeignKey(related_name='items', blank=True, to='koleso.GoodsVendor', null=True),
        ),
        migrations.AddField(
            model_name='disk',
            name='vendor',
            field=models.ForeignKey(related_name='items', blank=True, to='koleso.DiskVendor', null=True),
        ),
    ]
