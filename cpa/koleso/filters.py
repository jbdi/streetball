# coding: utf8
from __future__ import unicode_literals
from django import forms

import django_filters
from django_filters import ChoiceFilter, RangeFilter
from django_filters.widgets import RangeWidget

from cpa.koleso.models import Tire, Disk, DiskVendor, TireVendor


range_params = {'help_text': '', 'widget': RangeWidget(
    attrs={'class': 'form-control range'})}
choice_params = {'help_text': '', 'widget': forms.Select(
    attrs={'class': 'form-control'})}


# NOTE: cache it!
def disk_brands_choices():
    choices = list(DiskVendor.objects.values_list('id', 'name'))
    choices.insert(0, ['', '...'])
    return choices


# NOTE: cache it!
def tire_brands_choices():
    choices = list(TireVendor.objects.values_list('id', 'name'))
    choices.insert(0, ['', '...'])
    return choices


def tire_diameters_choices():
    choices = list(Disk.objects.order_by('diameter').filter(diameter__gte=12)
                   .values_list('diameter', flat=True).distinct())
    choices.insert(0, ['', '...'])
    return choices


class DiskFilter(django_filters.FilterSet):
    vendor = ChoiceFilter(label='производитель:',
                          choices=disk_brands_choices(), **choice_params)
    price = RangeFilter(label='цена (руб.):', **range_params)
    diameter = RangeFilter(label='диаметр (дюймы):', **range_params)

    class Meta:
        model = Disk
        fields = ['vendor', 'diameter', 'price']


class TireFilter(django_filters.FilterSet):
    vendor = ChoiceFilter(label='производитель:',
                          choices=tire_brands_choices(), **choice_params)
    price = RangeFilter(label='цена (руб.):', **range_params)
    diameter = RangeFilter(label='диаметр (дюймы):', **range_params)

    class Meta:
        model = Tire
        fields = ['vendor', 'diameter', 'price']
