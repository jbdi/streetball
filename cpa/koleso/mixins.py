# coding: utf8

from django.db import models

from cpa.koleso.fields import Fields


class PackageMixins(models.Model):
    dimension_unit = Fields.dimension_unit
    weight_unit = Fields.weight_unit
    quantity = Fields.quantity
    product_weight = Fields.product_weight
    package_height = Fields.package_height
    package_length = Fields.package_length
    package_weight = Fields.package_weight
    package_width = Fields.package_width

    class Meta:
        abstract = True


class ShippingMixins(models.Model):
    international_shipping_cost = Fields.international_shipping_cost
    international_shipping_cost_by_country_br = Fields.international_shipping_cost_by_country_br
    merchant_shipping_cost = Fields.merchant_shipping_cost
    delivery = Fields.delivery
    local_delivery_cost = Fields.local_delivery_cost
    shipping_from = Fields.shipping_from

    class Meta:
        abstract = True