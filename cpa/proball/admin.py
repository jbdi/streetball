# coding: utf-8
from django.contrib import admin

from django_mptt_admin.admin import DjangoMpttAdmin

from cpa.proball.models import Proball, ProballCatalogCategory


@admin.register(ProballCatalogCategory)
class ProballCategoryAdmin(DjangoMpttAdmin):
    pass


@admin.register(Proball)
class ProballAdmin(admin.ModelAdmin):
    list_display = ('name', 'param', 'price')
