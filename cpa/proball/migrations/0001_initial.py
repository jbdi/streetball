# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Proball',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country_of_origin', models.CharField(max_length=100, null=True, blank=True)),
                ('condition', models.CharField(max_length=64, null=True, blank=True)),
                ('enabled', models.BooleanField(default=False)),
                ('available', models.BooleanField(default=False)),
                ('picture', models.URLField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('oldprice', models.FloatField(default=0, max_length=10, null=True, blank=True)),
                ('price', models.FloatField(default=0, max_length=10, null=True, blank=True)),
                ('modified_time', models.DateTimeField(null=True, blank=True)),
                ('url', models.URLField(max_length=512, null=True, blank=True)),
                ('param', models.CharField(max_length=512, null=True, blank=True)),
                ('sales_notes', models.CharField(max_length=512, null=True, blank=True)),
                ('item_type', models.CharField(max_length=512, null=True, blank=True)),
                ('item_id', models.CharField(default=b'', max_length=128, blank=True)),
                ('name', models.CharField(max_length=512, null=True, blank=True)),
            ],
            options={
                'ordering': ('price',),
                'db_table': 'catalog_proball',
            },
        ),
        migrations.CreateModel(
            name='ProballCatalogCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=100, blank=True)),
                ('sort_pos', models.IntegerField(default=0, null=True)),
                ('popular', models.BooleanField(default=False)),
                ('seo_name', models.CharField(default=b'', max_length=100, blank=True)),
                ('seo_title', models.CharField(default=b'', max_length=250, blank=True)),
                ('seo_description', models.CharField(default=b'', max_length=250, blank=True)),
                ('seo_keywords', models.CharField(default=b'', max_length=250, blank=True)),
                ('text', models.TextField(default=b'', blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='category', blank=True, to='proball.ProballCatalogCategory', null=True)),
            ],
            options={
                'db_table': 'catalog_proballcategory',
            },
        ),
        migrations.AddField(
            model_name='proball',
            name='category',
            field=models.ForeignKey(related_name='proball_items', to='proball.ProballCatalogCategory', null=True),
        ),
    ]
