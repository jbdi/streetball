from .common import *


DEBUG = False
ALLOWED_HOSTS = ['localhost', '*.localhost', '*.wheelzard.ru']
SITE_ID = 67


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'wheelzard',
        'USER': 'wheelzard',
        'PASSWORD': '',
        'HOST': '127.0.0.1'
    }
}

INSTALLED_APPS += ('debug_toolbar',)

MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'cpa.debug_toolbar_fix.debug.TemplatesPanel', # original broken by django-jinja, remove this whole block later
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]

cache_backend = 'django.core.cache.backends.dummy.DummyCache'
CACHES = {
    'default': {
        'BACKEND': cache_backend,
        # 'LOCATION': '/var/tmp/django_cache',
        'TIMEOUT': 60 * 60,
        'OPTIONS': {
            'MAX_ENTRIES': 20000
        }
    }
}


TEMPLATES = [
    {
        'BACKEND': 'django_jinja.backend.Jinja2',
        'DIRS': [os.path.join(os.path.join(BASE_DIR, 'cpa'), 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            # Match the template names ending in .html but not the ones in the admin folder.
            "match_extension": ".jinja",
            "match_regex": r"^(?!admin/).*",
            "app_dirname": "templates",
            "bytecode_cache": {
                "name": "default",
                "backend": "django_jinja.cache.BytecodeCache",
                "enabled": False,
            },
            "autoescape": True,
            "auto_reload": DEBUG,
            # "translation_engine": "django.utils.translation",

            # Can be set to "jinja2.Undefined" or any other subclass.
            "undefined": None,
            "globals": {
                'bootstrap_css': 'bootstrap3.templatetags.bootstrap3.bootstrap_css',
                'bootstrap_javascript': 'bootstrap3.templatetags.bootstrap3.bootstrap_javascript',
            },
            # "extensions": DEFAULT_EXTENSIONS + [
            #     "endless_pagination.templatetags.endless.paginate"
            # ],
            'context_processors': [
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.static',
                "django.template.context_processors.request",
                "cpa.context_processors.cities",
            ],
        },
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'cpa', 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.static',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                "django.template.context_processors.request",
            ],
        },
    }
]
