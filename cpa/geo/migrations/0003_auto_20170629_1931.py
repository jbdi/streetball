# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0002_city_site'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='city',
            options={'ordering': ('ru_names',)},
        ),
        migrations.AlterUniqueTogether(
            name='city',
            unique_together=set([]),
        ),
    ]
