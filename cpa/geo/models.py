# coding: utf-8
from django.db import models

from django.contrib.sites.models import Site

from cities_light.abstract_models import (AbstractCity, AbstractRegion, AbstractCountry)
from cities_light.receivers import connect_default_signals


class Country(AbstractCountry):
    pass
connect_default_signals(Country)

class Region(AbstractRegion):
    pass
connect_default_signals(Region)


class City(AbstractCity):
    ru_names = models.CharField(max_length=128, blank=True, default='')
    case_where = models.CharField(max_length=128, blank=True, default='') # предложный падеж города
    site = models.ForeignKey(Site, null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ('ru_names',)
    # NOTE: добавить контакты филиала
connect_default_signals(City)


# import cities_light
# from cities_light.settings import ICity

# def set_city_fields(sender, instance, items, **kwargs):
#     instance.timezone = items[ICity.timezone]
# cities_light.signals.city_items_post_import.connect(set_city_fields)
