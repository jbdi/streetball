# coding: utf-8
from django.contrib import admin

from django_mptt_admin.admin import DjangoMpttAdmin

from cpa.avtogsm.models import Avtogsm, AvtogsmCatalogCategory


@admin.register(AvtogsmCatalogCategory)
class AvtogsmCategoryAdmin(DjangoMpttAdmin):
    pass


@admin.register(Avtogsm)
class AvtogsmAdmin(admin.ModelAdmin):
    readonly_fields = ('url', 'country_of_origin')
    fields = (
        ('category',),
        ('available', 'enabled'),
        ('name', 'item_id', 'description'),
        ('picture', 'url'),
        ('price', 'oldprice'),
        # others
        ('condition',),
        ('modified_time',),
        ('param',),
        (
            'sales_notes', 'item_type',
            'country_of_origin',
        ),
    )
    list_display = ('name', 'param', 'price')
    list_filter = ('category',)
    # prepopulated_fields = {'slug': ('title',)}
