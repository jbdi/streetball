# coding: utf-8
# from __future__ import unicode_literals
import csv
from datetime import datetime
from itertools import product
import re
import sys

from django.core.management.base import BaseCommand
from django.conf import settings

import pytz

from cpa.avtogsm.models import (AvtogsmCatalogCategory, Avtogsm)

# """
# Скачивание файла
# Импорт файла
#     - подсчет кол-ва товаров в файле
# """

ROOT_CATEGORY_ID = 1

IMPORTED_ITEMS_ID = []

DEBUG_MODE = False

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--debug',
            action='store',
            dest='debug',
            default=False,
            help='Turn on debug mode')

    # сделать отдельной процедурой подсчет(перерасчет) кол-ва товаров в категории
    def handle(self, *args, **options):
        csv_reader('avtogsm.csv', run_import, **options)
        Avtogsm.objects.exclude(id__in=IMPORTED_ITEMS_ID).update(enabled=False)




def run_import(row, **options):
    DEBUG_MODE = options.get('debug')
    root, _ = AvtogsmCatalogCategory.objects.get_or_create(name='main_root')

    ALLOWED_FIELDS = (
        'available',
        'picture',
        'name',
        'oldprice',
        'price',
        'url',
        'modified_time',
        'category_id',
        'item_type',
        'item_id',
        'local_delivery_cost',
        'description',
    )

    row_columns = row.keys()
    for field_name in row_columns:
        if not field_name in ALLOWED_FIELDS:
            del row[field_name]


    row['category_id'] = make_categories_tree(row['category_id'])
    row['modified_time'] = datetime.utcfromtimestamp(int(row['modified_time'])).replace(tzinfo=pytz.utc)
    model = Avtogsm

    if not row:
        return {'created': 0, 'updated': 0, 'handled': 1}

    row['price'] = float(row['price'].replace(',', '.') or 0)
    row['enabled'] = True
    row['oldprice'] = float(row['oldprice'].replace(',', '.') or 0)

    row = {col: val for col, val in row.items()}

    # row.update()
    # row_analizer(row)

    # try:
    #     # IMPORTED_ITEMS_ID.append(item.id)
    # except Exception, e:
    #     print 'error "%s" with %s' % (e.message, row.get('item_id'))
    #     created = False

    if not DEBUG_MODE:
        try:
            item, created = model.objects.update_or_create(item_id=row['item_id'], defaults=row)
        except ValueError, e:
            pass
    else:
        created = False

    return get_counters_dict(DEBUG_MODE, created)


def row_analizer(row):
    def match_gender(row):
        WOMEN = [u'женска', u'женски', u'женских', u'женской', u'женского', u'женские', u'женская', \
               u'женское', u'женщин', u'женщину', u'женщине', u'женщины', u'женщинам', u'женскую', u'женский']
        MEN = [u'мужска', u'мужски', u'мужских', u'мужской', u'мужского', u'мужские', u'мужская', \
               u'мужское', u'мужчин', u'мужчину', u'мужчине', u'мужчины', u'мужчинам', u'мужскую']
        CHILDREN = [u'детских', u'детский', u'детскую', u'детской', u'детского', u'детские', u'детская', \
                    u'детское', u'детей', u'детям', u'дети', u'ребенка', u'ребенку']
        WOMAN = u'для женщин'
        MAN = u'для мужчин'
        KID = u'для детей'

        GENDER_MAP = (
            tuple(product(WOMEN, [WOMAN]) )+ \
            tuple(product(MEN, [MAN])) + \
            tuple(product(CHILDREN, [KID]))
        )
        text = row['name'] + ' ' + row['description']
        text = text.decode(encoding='utf8')

        gender_re = ur'(женск[а-яё]*|женщин[ы]?|детск[а-яё]*|детей|мужск[а-яё]*|мужчин[а-яё]*)'
        matches = re.search(gender_re, text, re.MULTILINE|re.I|re.UNICODE)

        result = matches.groups()[0] if matches else None
        if result:
            try:
                gender = dict(GENDER_MAP)[result.lower()]
            except KeyError, e:
                print 'error key: %s' % result.lower()
        return gender

    return {}


def get_counters_dict(DEBUG_MODE, created):
    counters = {}
    counters['handled'] = 1
    if not DEBUG_MODE:
        if created:
            counters['created'] = 1
            counters['updated'] = 0
        else:
            counters['created'] = 0
            counters['updated'] = 1
    else:
        counters['created'] = 0
        counters['updated'] = 0
    return counters


def get_number_from_str(string):
    return re.split('([\d\.]+)', string, maxsplit=1, flags=re.U|re.I)[1]


def make_categories_tree(categories_string=''):
    """
    делает из строки "Category/subcategory/sub subcategory" дерево категорий в бд
    """
    parent = AvtogsmCatalogCategory.objects.get(id=ROOT_CATEGORY_ID)
    categories = enumerate(categories_string.split('/'), start=0)
    for index, category_name in categories:
        category, created = AvtogsmCatalogCategory.objects.get_or_create(name=category_name, parent=parent)
        parent = category
    return category.id


def csv_reader(file_name, row_handler, **options):
    verbosity = options.get('verbosity', 0)
    file_path = settings.BASE_DIR + '/' + file_name

    with open(file_path, 'r') as line:
        reader = csv.DictReader(line, delimiter=';')

        # # также можно припихнуть проверку на слишком маленкое кол-во товаров
        # cursor.execute('truncate catalog_item')
        handled = 0
        created = 0
        updated = 0
        for row in reader:
            counters = row_handler(row, **options)
            handled += counters['handled']
            created += counters['created']
            updated += counters['updated']
            if verbosity == 2:
                sys.stdout.write("\rhandled: %d, created: %d, updated: %d" % (handled, created, updated))
                sys.stdout.flush()
        if verbosity == 1:
            sys.stdout.write("\rhandled: %d, created: %d, updated: %d" % (handled, created, updated))
