# coding: utf-8
# from __future__ import unicode_literals
import csv
from datetime import datetime
from itertools import product
import re
import sys

from django.core.management.base import BaseCommand
from django.conf import settings

import pytz

from cpa.vseinstrumenti.models import (VseinstrumentiCategory, Vseinstrumenti)

# """
# Скачивание файла
# Импорт файла
#     - подсчет кол-ва товаров в файле
# """

ROOT_CATEGORY_ID = 1

IMPORTED_ITEMS_ID = []

DEBUG_MODE = False

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--debug',
            action='store',
            dest='debug',
            default=True,
            help='Turn on debug mode')

    # сделать отдельной процедурой подсчет(перерасчет) кол-ва товаров в категории
    def handle(self, *args, **options):
        csv_reader('vseinstrumenti.csv', run_import, **options)
        Vseinstrumenti.objects.exclude(id__in=IMPORTED_ITEMS_ID).update(enabled=False)




def run_import(row, **options):
    DEBUG_MODE = int(options.get('debug'))
    root, _ = VseinstrumentiCategory.objects.get_or_create(name='root')

    ALLOWED_FIELDS = (
        'available',
        'picture',
        'description',
        'oldprice',
        'url',
        'price',
        'modified_time',
        'item_id',
        'item_type',
        'category_id',
        'name',
    )


    row_columns = row.keys()
    for field_name in row_columns:
        if not field_name in ALLOWED_FIELDS:
            del row[field_name]


    row['category_id'] = make_categories_tree(row['category_id'])
    row['modified_time'] = datetime.utcfromtimestamp(int(row['modified_time'])).replace(tzinfo=pytz.utc)
    model = Vseinstrumenti

    if not row:
        return {'created': 0, 'updated': 0, 'handled': 1}

    row['price'] = float(row['price'].replace(',', '.') or 0)
    row['oldprice'] = float(row['oldprice']) if row['oldprice'] else 0.0
    row['enabled'] = True
    row['description'] = row['description'].decode(encoding='utf8')
    row['name'] = row['name'].decode(encoding='utf8')

    row = {col: val for col, val in row.items()}

    # row.update()
    row_analizer(row)

    # try:
    #     # IMPORTED_ITEMS_ID.append(item.id)
    # except Exception, e:
    #     print 'error "%s" with %s' % (e.message, row.get('item_id'))
    #     created = False
    if not DEBUG_MODE:
        item, created = model.objects.update_or_create(item_id=row['item_id'], defaults=row)
    else:
        created = False

    return get_counters_dict(DEBUG_MODE, created)


def row_analizer(row):
    return {}


def get_counters_dict(DEBUG_MODE, created):
    counters = {}
    counters['handled'] = 1
    if not DEBUG_MODE:
        if created:
            counters['created'] = 1
            counters['updated'] = 0
        else:
            counters['created'] = 0
            counters['updated'] = 1
    else:
        counters['created'] = 0
        counters['updated'] = 0
    return counters


def get_number_from_str(string):
    return re.split('([\d\.]+)', string, maxsplit=1, flags=re.U|re.I)[1]


def make_categories_tree(categories_string=''):
    """
    делает из строки "Category/subcategory/sub subcategory" дерево категорий в бд
    """
    parent = VseinstrumentiCategory.objects.get(id=ROOT_CATEGORY_ID)
    categories = enumerate(categories_string.split('/'), start=0)
    for index, category_name in categories:
        category, created = VseinstrumentiCategory.objects.get_or_create(name=category_name, parent=parent)
        parent = category
    return category.id


def csv_reader(file_name, row_handler, **options):
    verbosity = options.get('verbosity', 0)
    file_path = settings.BASE_DIR + '/' + file_name

    with open(file_path, 'r') as line:
        reader = csv.DictReader(line, delimiter=';')

        # # также можно припихнуть проверку на слишком маленкое кол-во товаров
        # cursor.execute('truncate catalog_item')
        handled = 0
        created = 0
        updated = 0
        for row in reader:
            counters = row_handler(row, **options)
            handled += counters['handled']
            created += counters['created']
            updated += counters['updated']
            if verbosity == 2:
                sys.stdout.write("\rhandled: %d, created: %d, updated: %d" % (handled, created, updated))
                sys.stdout.flush()
        if verbosity == 1:
            sys.stdout.write("\rhandled: %d, created: %d, updated: %d" % (handled, created, updated))
