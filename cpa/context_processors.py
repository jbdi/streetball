# coding: utf-8
from django.contrib.sites.shortcuts import get_current_site
from cpa.geo.models import City


def cities(request):
    current_site = get_current_site(request)
    city = City.objects.get(site=current_site)

    return {
        'city': city,
        'city_ru': city.ru_names,
        'city_where': city.case_where,
    }
